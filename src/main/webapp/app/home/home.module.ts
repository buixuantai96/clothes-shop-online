import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ClothesShopOnlineSharedModule } from 'app/shared';
import { HOME_ROUTE, HomeComponent } from './';
import {ClothesShopOnlineProductModule} from 'app/entities/product/product.module';

@NgModule({
    imports: [ClothesShopOnlineSharedModule, ClothesShopOnlineProductModule, RouterModule.forChild([HOME_ROUTE])],
    declarations: [HomeComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ClothesShopOnlineHomeModule {}
