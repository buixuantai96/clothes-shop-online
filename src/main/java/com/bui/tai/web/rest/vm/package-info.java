/**
 * View Models used by Spring MVC REST controllers.
 */
package com.bui.tai.web.rest.vm;
