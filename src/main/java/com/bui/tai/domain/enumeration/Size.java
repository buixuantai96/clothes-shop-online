package com.bui.tai.domain.enumeration;

/**
 * The Size enumeration.
 */
public enum Size {
    S, M, L, XL, XXL
}
