import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: 'product',
                loadChildren: './product/product.module#ClothesShopOnlineProductModule'
            },
            {
                path: 'product-category',
                loadChildren: './product-category/product-category.module#ClothesShopOnlineProductCategoryModule'
            },
            {
                path: 'customer',
                loadChildren: './customer/customer.module#ClothesShopOnlineCustomerModule'
            },
            {
                path: 'product-order',
                loadChildren: './product-order/product-order.module#ClothesShopOnlineProductOrderModule'
            },
            {
                path: 'order-item',
                loadChildren: './order-item/order-item.module#ClothesShopOnlineOrderItemModule'
            },
            {
                path: 'invoice',
                loadChildren: './invoice/invoice.module#ClothesShopOnlineInvoiceModule'
            },
            {
                path: 'shipment',
                loadChildren: './shipment/shipment.module#ClothesShopOnlineShipmentModule'
            }
            /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
        ])
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ClothesShopOnlineEntityModule {}
