package com.bui.tai.domain.enumeration;

/**
 * The OrderStatus enumeration.
 */
public enum OrderStatus {
    COMPLETED, PENDING, CANCELLED
}
